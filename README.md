# ATtiny85 Breakout w/ CP2102 USB to TTL header v1.0 #

## Schematic ##

![Schematic.PNG](https://bytebucket.org/serisman/pcb-attiny85-breakout-cp2102/raw/master/output/Schematic.PNG)

## Design Rules ##

* Clearance: 7 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 7 mil
* Zone Min Width: 7 mil
* Zone Thermal Antipad Clearance: 7 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.6" x 0.6" (15.27 mm x 15.27 mm)
* $0.60 each ($1.80 for 3)
* [https://oshpark.com/shared_projects/YZ1uykvs](https://oshpark.com/shared_projects/YZ1uykvs)

### PCB Front ###

![PCB-Front.png](https://bytebucket.org/serisman/pcb-attiny85-breakout-cp2102/raw/master/output/PCB-Front.png)

### PCB Back ###

![PCB-Back.png](https://bytebucket.org/serisman/pcb-attiny85-breakout-cp2102/raw/master/output/PCB-Back.png)